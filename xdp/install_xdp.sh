#!/bin/bash

container=${1:-spine1}
interface=${2:-eth0}

set -e

make

echo "installing xdp on $interface @ $container ..."

docker cp xdp_router.o $container:/
docker cp xdp_loader $container:/
docker cp xdp_tunnels $container:/
docker cp tunnels.sh $container:/
interface=eth3
docker exec $container /bin/bash -c "ulimit -l 1024 && /xdp_loader -d $interface --auto-mode --force --filename /xdp_router.o --progsec xdp_l2tpv3 && /tunnels.sh | /xdp_tunnels --dev $interface"
interface=eth4
docker exec $container /bin/bash -c "ulimit -l 1024 && /xdp_loader -d $interface --auto-mode --force --filename /xdp_router.o --progsec xdp_l2tpv3 && /tunnels.sh | /xdp_tunnels --dev $interface"
