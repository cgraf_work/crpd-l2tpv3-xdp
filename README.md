# crpd-l2tpv3-xdp

Demo/prototype work to encap/decap and route between unmanaged L2TPv3 tunnels.

- [xdp](xdp): xdp source compiled into a container xdpbuild
- [l2tpv3router](l2tpv3router): Ubuntu container with xdp binaries and configured tshark
- [crpd_l2tpv3](crpd_l2tpv3): XDP augmented cRPD container
- [pktgen](pktgen): DPDK pktgen compiled into a container
- [testbench](testbench): Various test scenario's (functional, performance, with and without cRPD)

## Clone repo

This repo contains the submodule libbpf, so use 

```
$ git clone --recursive 
```

If already cloned, use `git submodule update --init` to fix the mistake.

## Requirements

- make, docker and docker-compose installed
- Juniper cRPD 20.2R1.10: Download and install latest Juniper cRPD version and load into docker. Check https://www.juniper.net/us/en/dm/crpd-trial/ 
for a free trial download.
- Juniper cRPD licenses

## References

- https://blog.apnic.net/2020/04/30/how-to-build-an-xdp-based-bgp-peering-router/
- https://levelup.gitconnected.com/building-a-high-performance-linux-based-traffic-generator-with-dpdk-93bb9904416c
- https://github.com/xdp-project/xdp-tutorial
- [XDP MTU limitation description from DPDK](http://doc.dpdk.org/guides/nics/af_xdp.html)
- [An approximate list of drivers or components supporting XDP programs](https://github.com/iovisor/bcc/blob/master/docs/kernel-versions.md#xdp)
- Using GRE tunnel with cRPD: https://www.juniper.net/documentation/en_US/release-independent/nce/topics/example/crpd.html
- [xdp-tutorial about VLAN and XDP (hint disable VLAN offload!)](https://github.com/xdp-project/xdp-tutorial/tree/master/packet01-parsing#a-note-about-vlan-offloads)
- https://www.comsys.rwth-aachen.de/fileadmin/papers/2019/2019-hohlfeld-bpfperf.pdf
- https://penberg.org/papers/xdp-steering-encp19.pdf
- https://www.slideshare.net/suselab/ixgbe-internals
- Add support for XDP in egress path: https://lwn.net/Articles/813406/
- [Fast Packet Processing with eBPF and XDP Concepts, Code Challenges and Applications, ACMJATS Dec 2019](https://www.researchgate.net/profile/Marcos_Vieira3/publication/339084847_Fast_Packet_Processing_with_eBPF_and_XDP_Concepts_Code_Challenges_and_Applications/links/5e4145f592851c7f7f2c28eb/Fast-Packet-Processing-with-eBPF-and-XDP-Concepts-Code-Challenges-and-Applications.pdf?origin=publication_detail)
- [Intel NIC setup](https://suricata.readthedocs.io/en/suricata-5.0.3/capture-hardware/ebpf-xdp.html#intel-nic-setup)


## Demo Topology

```mermaid                                                                          
graph TB
spline1["spine1 10.0.0.1"]---|l2tpv3|r1["r1 10.0.0.11"]
spline1---|l2tpv3|r2["r2 10.0.0.12"]
spline1---|l2tpv3|r3["r3 10.0.0.13"]
```

Star topology of 4 Juniper cRPD instances, augmented with L2TPv3 tunnel configuration between IPv6 loopback addresses. ISIS is used to learn the neighbors v6 loopback addresses. BGPv6 peering is then established between v6 loopbacks. L2TPv3 tunnels are learned by cRPD and v4 routes, including loopbacks shown in the diagram above, are announced via BGPv4 peering thru the tunnels. 

### Build and launch topology

```
$ make up

make -C xdp
make[1]: Entering directory '/home/mwiget/crpd-l2tpv3-xdp/xdp'
docker build -t xdpbuild .
Sending build context to Docker daemon  3.483MB
Step 1/17 : FROM ubuntu:20.04 as libbpf
 ---> 1e4467b07108
Step 2/17 : RUN apt-get update   && apt-get install --no-install-recommends -y build-essential clang llvm     libelf-dev
 ---> Using cache
 ---> 5b59833bfe13
Step 3/17 : COPY libbpf /libbpf/
 ---> Using cache
 ---> 805fed3b5b4f
Step 4/17 : RUN test -d /libbpf/src || (echo "\nuse 'git submodule update --init' to popluate libbpf/\n" && exit 1)
 ---> Using cache
 ---> 633620f134fd
Step 5/17 : WORKDIR /libbpf/src
 ---> Using cache
 ---> 27d721d32032
Step 6/17 : RUN mkdir -p build root   && NO_PKG_CONFIG=1 BUILD_STATIC_ONLY=y OBJDIR=build DESTDIR=root make install
 ---> Using cache
 ---> 50b0e9cda79a
Step 7/17 : RUN ls -lR /libbpf/src/root
 ---> Using cache
 ---> 5b0433f52c64
Step 8/17 : FROM ubuntu:20.04
 ---> 1e4467b07108
Step 9/17 : RUN apt-get update   && apt-get install --no-install-recommends -y build-essential clang llvm     libelf-dev gcc-multilib
 ---> Using cache
 ---> 9f437e13461a
Step 10/17 : COPY --from=libbpf /libbpf/src/root/ /
 ---> Using cache
 ---> b21616d32d37
Step 11/17 : COPY *.[ch] /
 ---> Using cache
 ---> e4e25ffbef08
Step 12/17 : COPY tunnels.sh /
 ---> Using cache
 ---> 1265a8dbb058
Step 13/17 : RUN clang -Wall -O2 -lz -lelf -o xdp_loader xdp_loader.c common_params.c common_user_bpf_xdp.c /usr/lib64/libbpf.a
 ---> Using cache
 ---> 1e43372d0b64
Step 14/17 : RUN clang -Wall -O2 -lz -lelf -o xdp_stats xdp_stats.c common_params.c common_user_bpf_xdp.c /usr/lib64/libbpf.a
 ---> Using cache
 ---> 7b161e4063bf
Step 15/17 : RUN clang -Wall -O2 -lz -lelf -o xdp_tunnels xdp_tunnels.c common_params.c common_user_bpf_xdp.c /usr/lib64/libbpf.a
 ---> Using cache
 ---> 4db99b303573
Step 16/17 : RUN strip xdp_loader xdp_stats xdp_tunnels
 ---> Using cache
 ---> f2e8941c3198
Step 17/17 : RUN clang -O2 -g -c -target bpf -Wno-compare-distinct-pointer-types -o xdp_router.o xdp_router.c   && llvm-objdump -S xdp_router.o
 ---> Using cache
 ---> ba1cab05d97b
Successfully built ba1cab05d97b
Successfully tagged xdpbuild:latest
./extract.sh
extracting xdp tools from xdpbuild ...
xdpbuild
3ec3be762a12c80674fed590389615173cf90f5c16c68d357d05d9186d29e778
-rwxr-xr-x 1 mwiget mwiget   1037 Aug  4 09:05 tunnels.sh
-rwxr-xr-x 1 mwiget mwiget 171024 Aug  4 09:20 xdp_loader
-rw-r--r-- 1 mwiget mwiget  68744 Aug  4 09:20 xdp_router.o
-rwxr-xr-x 1 mwiget mwiget 171056 Aug  4 09:20 xdp_stats
-rwxr-xr-x 1 mwiget mwiget 171024 Aug  4 09:20 xdp_tunnels
make[1]: Leaving directory '/home/mwiget/crpd-l2tpv3-xdp/xdp'
make -C l2tpv3router
make[1]: Entering directory '/home/mwiget/crpd-l2tpv3-xdp/l2tpv3router'
docker build -t l2tpv3router .
Sending build context to Docker daemon  9.216kB
Step 1/10 : FROM xdpbuild as xdp
 ---> ba1cab05d97b
Step 2/10 : FROM ubuntu:20.04
 ---> 1e4467b07108
Step 3/10 : RUN apt-get update   && apt-get -y --no-install-recommends install   netsniff-ng iproute2 net-tools iputils-ping   tcpdump tshark bwm-ng ethtool fping vim-tiny   && rm -rf /var/lib/apt/lists/*
 ---> Using cache
 ---> d900f0eab962
Step 4/10 : COPY wireshark /root/.config/wireshark/
 ---> Using cache
 ---> 26041611fba8
Step 5/10 : COPY tshark /usr/local/bin/tshark
 ---> Using cache
 ---> 71431f95630b
Step 6/10 : COPY --from=xdp /xdp_stats /xdp_loader /xdp_tunnels /sbin/
 ---> Using cache
 ---> 0765e424e3c4
Step 7/10 : COPY --from=xdp /xdp_router.o /tunnels.sh /root/
 ---> Using cache
 ---> ddec9f482039
Step 8/10 : COPY pcap_decode_l2tpv3.sh /root/
 ---> Using cache
 ---> 39855269c764
Step 9/10 : RUN chmod a+rx /root/*.sh /usr/local/bin/tshark
 ---> Using cache
 ---> 90bb6b381ee5
Step 10/10 : WORKDIR /root
 ---> Using cache
 ---> f9368480dda4
Successfully built f9368480dda4
Successfully tagged l2tpv3router:latest
make[1]: Leaving directory '/home/mwiget/crpd-l2tpv3-xdp/l2tpv3router'
make -C crpd_l2tpv3
make[1]: Entering directory '/home/mwiget/crpd-l2tpv3-xdp/crpd_l2tpv3'
docker build -t crpd_l2tpv3 .
Sending build context to Docker daemon  8.192kB
Step 1/11 : FROM xdpbuild as xdp
 ---> ba1cab05d97b
Step 2/11 : FROM crpd:20.2R1.10
 ---> 41d6ae0a8fcb
Step 3/11 : RUN apt-get update   && apt-get -y --no-install-recommends install bwm-ng ethtool tshark   && rm -rf /var/lib/apt/lists/*
 ---> Using cache
 ---> d90cc8554eb3
Step 4/11 : COPY wireshark /root/.config/wireshark/
 ---> Using cache
 ---> a9ce9c20acd6
Step 5/11 : COPY tshark.sh /root/tshark.sh
 ---> Using cache
 ---> 6e62887e62a1
Step 6/11 : COPY --from=xdp /xdp_stats /xdp_tunnels /xdp_loader /sbin/
 ---> Using cache
 ---> 5326e6465031
Step 7/11 : COPY --from=xdp /tunnels.sh /xdp_router.o /root/
 ---> Using cache
 ---> 9b27aec0298f
Step 8/11 : COPY runit-init.sh /sbin/
 ---> Using cache
 ---> 7cd1488d9b76
Step 9/11 : RUN chmod +x /sbin/runit-init.sh /root/tshark.sh
 ---> Using cache
 ---> acead7487884
Step 10/11 : WORKDIR /root
 ---> Using cache
 ---> 987360a5820d
Step 11/11 : STOPSIGNAL 35
 ---> Using cache
 ---> ed5f40aa760e
Successfully built ed5f40aa760e
Successfully tagged crpd_l2tpv3:latest
make[1]: Leaving directory '/home/mwiget/crpd-l2tpv3-xdp/crpd_l2tpv3'
make -C pktgen
make[1]: Entering directory '/home/mwiget/crpd-l2tpv3-xdp/pktgen'
docker build -t pktgen .
Sending build context to Docker daemon  19.87MB
Step 1/29 : ARG dpdk_version=20.02
Step 2/29 : ARG pktgen_version=20.02.0
Step 3/29 : FROM ubuntu:18.04 as build
 ---> 2eb2d388e1a2
Step 4/29 : ARG dpdk_version
 ---> Using cache
 ---> 928ab8329a74
Step 5/29 : ARG pktgen_version
 ---> Using cache
 ---> 371776903eca
Step 6/29 : RUN apt-get update   && apt-get -y install build-essential python3-pip liblua5.3-dev   wget libnuma-dev pciutils libpcap-dev libelf-dev linux-headers-`uname -r`
 ---> Using cache
 ---> 595c219559f2
Step 7/29 : RUN ln -s /usr/bin/python3 /usr/bin/python
 ---> Using cache
 ---> aa0b4d5c9f49
Step 8/29 : ENV DPDK_VER=$dpdk_version
 ---> Using cache
 ---> c8cbe2019137
Step 9/29 : ENV PKTGEN_VER=$pktgen_version
 ---> Using cache
 ---> a27dd9ed8a72
Step 10/29 : ENV RTE_SDK=/opt/dpdk-$DPDK_VER
 ---> Using cache
 ---> 8c7bbe0f52ac
Step 11/29 : ENV RTE_TARGET=x86_64-native-linuxapp-gcc
 ---> Using cache
 ---> 591dc011529e
Step 12/29 : RUN echo DPDK_VER=${DPDK_VER}
 ---> Using cache
 ---> 64577ccb3a7b
Step 13/29 : RUN echo PKTGEN_VER=${PKTGEN_VER}
 ---> Using cache
 ---> f501207edd1b
Step 14/29 : WORKDIR /opt
 ---> Using cache
 ---> 770109dee80f
Step 15/29 : RUN wget -q https://fast.dpdk.org/rel/dpdk-$DPDK_VER.tar.xz && tar xf dpdk-$DPDK_VER.tar.xz
 ---> Using cache
 ---> 837fdb9bc0fa
Step 16/29 : RUN cd $RTE_SDK    && make config T=$RTE_TARGET CONFIG_RTE_EAL_IGB_UIO=y    && make install T=$RTE_TARGET DESTDIR=install CONFIG_RTE_EAL_IGB_UIO=y
 ---> Using cache
 ---> edbbfea188a2
Step 17/29 : RUN wget -q https://dpdk.org/browse/apps/pktgen-dpdk/snapshot/pktgen-$PKTGEN_VER.tar.gz    && tar xf pktgen-$PKTGEN_VER.tar.gz
 ---> Using cache
 ---> 1b9cf3b6f9bd
Step 18/29 : RUN cd pktgen-$PKTGEN_VER && make
 ---> Using cache
 ---> aae2ecdacaaf
Step 19/29 : RUN ls -l /opt/pktgen-$PKTGEN_VER/app/$RTE_TARGET/
 ---> Using cache
 ---> 4a322478cb6f
Step 20/29 : RUN ls -l /opt/dpdk-$DPDK_VER/x86_64-native-linuxapp-gcc/kmod/*uio.ko
 ---> Using cache
 ---> 54572f1a330b
Step 21/29 : FROM ubuntu:18.04
 ---> 2eb2d388e1a2
Step 22/29 : ARG dpdk_version
 ---> Using cache
 ---> 928ab8329a74
Step 23/29 : ENV DPDK_VER=$dpdk_version
 ---> Using cache
 ---> 24bcf84e38c0
Step 24/29 : RUN apt-get update   && apt-get -y --no-install-recommends install liblua5.3 libnuma-dev pciutils libpcap-dev
 ---> Using cache
 ---> 80df5f39dd4f
Step 25/29 : RUN ln -s /usr/bin/python3 /usr/bin/python
 ---> Using cache
 ---> 7e9c4a4ae035
Step 26/29 : COPY --from=build /opt/dpdk-$DPDK_VER/x86_64-native-linuxapp-gcc/kmod/igb_uio.ko /dpdk/
 ---> Using cache
 ---> 4056e854e377
Step 27/29 : COPY --from=build /opt/pktgen-$DPDK_VER.0/app/x86_64-native-linuxapp-gcc/pktgen /dpdk/
 ---> Using cache
 ---> e884799dd2dd
Step 28/29 : COPY --from=build /opt/dpdk-$DPDK_VER/usertools /dpdk/usertools/
 ---> Using cache
 ---> 9f1744399152
Step 29/29 : WORKDIR /dpdk
 ---> Using cache
 ---> 9b02fad960e3
Successfully built 9b02fad960e3
Successfully tagged pktgen:latest
make[1]: Leaving directory '/home/mwiget/crpd-l2tpv3-xdp/pktgen'
docker-compose down
docker-compose up -d
scripts/add_link.sh spine1 r1
spine1 b88471467e92
r1 c80eea9a3751
spine1 has pid 14162
r1 has pid 14169
spine1 has 0 eth interfaces
r1 has 0 eth interfaces
vspine1-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 9a:d2:5b:ad:c7:ce  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vr1-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether f6:7d:38:32:24:a6  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

setting mtu ...
moving endpoints to netns ...
bringing links up ...
spine1:eth0 === r1:eth0
scripts/add_link.sh spine1 r2
spine1 b88471467e92
r2 1360cbf9e95c
spine1 has pid 14162
r2 has pid 14171
spine1 has 1 eth interfaces
r2 has 0 eth interfaces
vspine1-1: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 0e:87:c6:a6:6b:45  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vr2-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 12:a4:b5:a8:f4:11  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

setting mtu ...
moving endpoints to netns ...
bringing links up ...
spine1:eth1 === r2:eth0
scripts/add_link.sh spine1 r3
spine1 b88471467e92
r3 a9c1e771b712
spine1 has pid 14162
r3 has pid 14136
spine1 has 2 eth interfaces
r3 has 0 eth interfaces
vspine1-2: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 82:d5:a5:70:ff:75  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vr3-0: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 76:c8:34:7e:cf:fe  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

setting mtu ...
moving endpoints to netns ...
bringing links up ...
spine1:eth2 === r3:eth0
./validate.sh
0: waiting for all isis adjancencies up on spine1 ...
2: waiting for all isis adjancencies up on spine1 ...
5: waiting for all isis adjancencies up on spine1 ...
7: waiting for all isis adjancencies up on spine1 ...
9: waiting for all isis adjancencies up on spine1 ...
11: waiting for all isis adjancencies up on spine1 ...
13: waiting for all isis adjancencies up on spine1 ...
15: waiting for all isis adjancencies up on spine1 ...
17: waiting for all isis adjancencies up on spine1 ...
20: waiting for all isis adjancencies up on spine1 ...
22: waiting for all isis adjancencies up on spine1 ...
24: waiting for all isis adjancencies up on spine1 ...
26: waiting for all isis adjancencies up on spine1 ...
28: waiting for all isis adjancencies up on spine1 ...
30: waiting for all isis adjancencies up on spine1 ...
32: waiting for all isis adjancencies up on spine1 ...
35: waiting for all isis adjancencies up on spine1 ...
37: waiting for all isis adjancencies up on spine1 ...
Interface             System         L State        Hold (secs) SNPA
eth0                  r1             2  Up                    6
eth1                  r2             2  Up                    8
eth2                  r3             2  Up                    8

39: waiting for all established bgp sessions on spine1 ...
41: waiting for all established bgp sessions on spine1 ...
43: waiting for all established bgp sessions on spine1 ...
Threading mode: BGP I/O
Groups: 2 Peers: 6 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       9          3          0          0          0          0
inet6.0              
                      27          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
192.168.101.2    4259905011          4          5       0       0           2 Establ
  inet.0: 1/3/3/0
192.168.102.2    4259905012          4          5       0       0           7 Establ
  inet.0: 1/3/3/0
192.168.103.2    4259905013          4          5       0       0           7 Establ
  inet.0: 1/3/3/0
fd00::11         4259905011          4          4       0       0           6 Establ
  inet6.0: 0/7/7/0
fd00::12         4259905012          5          5       0       0          10 Establ
  inet6.0: 0/9/9/0
fd00::13         4259905013          7          7       0       0          34 Establ
  inet6.0: 0/11/11/0

PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
64 bytes from 10.0.0.1: icmp_seq=1 ttl=64 time=0.058 ms

--- 10.0.0.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.058/0.058/0.058/0.000 ms

PING 10.0.0.12 (10.0.0.12) 56(84) bytes of data.
64 bytes from 10.0.0.12: icmp_seq=1 ttl=63 time=0.060 ms

--- 10.0.0.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.060/0.060/0.060/0.000 ms

PING 10.0.0.13 (10.0.0.13) 56(84) bytes of data.
64 bytes from 10.0.0.13: icmp_seq=1 ttl=63 time=0.074 ms

--- 10.0.0.13 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.074/0.074/0.074/0.000 ms

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:44 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.251105
XDP_DROP               8 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.251138
XDP_PASS              36 pkts (         2 pps)           3 Kbytes (     0 Mbits/s) period:2.251138
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.251138
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.251138


Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:47 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250368
XDP_DROP               7 pkts (         0 pps)           1 Kbytes (     0 Mbits/s) period:2.250400
XDP_PASS              34 pkts (         0 pps)           3 Kbytes (     0 Mbits/s) period:2.250399
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250398
XDP_REDIRECT           2 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250397


show isis adj on spine1 ...
Interface             System         L State        Hold (secs) SNPA
eth0                  r1             2  Up                    8
eth1                  r2             2  Up                    6
eth2                  r3             2  Up                    7

show bgp summary on spine1 ...
Threading mode: BGP I/O
Groups: 2 Peers: 6 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                       9          3          0          0          0          0
inet6.0              
                      33          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
192.168.101.2    4259905011          4          5       0       0           7 Establ
  inet.0: 1/3/3/0
192.168.102.2    4259905012          4          5       0       0          12 Establ
  inet.0: 1/3/3/0
192.168.103.2    4259905013          4          5       0       0          12 Establ
  inet.0: 1/3/3/0
fd00::11         4259905011          5          4       0       0          11 Establ
  inet6.0: 0/11/11/0
fd00::12         4259905012          6          5       0       0          15 Establ
  inet6.0: 0/11/11/0
fd00::13         4259905013          7          7       0       0          39 Establ
  inet6.0: 0/11/11/0

show ip route hidden extensive on spine1 ...

inet.0: 12 destinations, 20 routes (12 active, 0 holddown, 0 hidden)

iso.0: 1 destinations, 1 routes (1 active, 0 holddown, 0 hidden)

inet6.0: 23 destinations, 57 routes (23 active, 0 holddown, 0 hidden)

success in 51 seconds
```

### Tear down

```
$ make down
```

