#!/bin/bash

PCI="0000:02:00.0"    # enp2s0 on ody

sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI
sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI
sudo python3 dpdk/usertools/dpdk-devbind.py -s

echo "allocating hugepages ..."
echo 512 | sudo tee /proc/sys/vm/nr_hugepages
