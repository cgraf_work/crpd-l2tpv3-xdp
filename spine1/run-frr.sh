#!/bin/bash

set -e
mount -t bpf bpf /sys/fs/bpf/

if [ -s /etc/frr/network-init.sh ]; then
   /bin/bash /etc/frr/network-init.sh > /root/network-init.log 2>&1 & disown
fi

cat > /etc/frr/daemons <<EOF
bgpd=yes
isisd=yes
vtysh_enable=yes
EOF

cat > /usr/bin/cli << EOF
#!/bin/sh
exit 1
EOF
chmod +x /usr/bin/cli

/usr/lib/frr/frrinit.sh start

tail -f /dev/null
