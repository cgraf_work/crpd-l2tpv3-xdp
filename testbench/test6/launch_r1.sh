#!/bin/bash

cookie="1122334455667788"
#set -e

interface=${1:-eth0}
exit_container=${2:-no}

mount -t bpf bpf /sys/fs/bpf/

until ip link show $interface up; do
   echo "waiting for $interface up ..."
   sleep 1
done

ip -6 addr add fd00::1/64 dev $interface
ip -6 route add fd00::/16 via fd00::2

ip -6 addr add fd00:1::101/128 dev lo
ip -6 addr add fd00:1::102/128 dev lo
ip -6 addr add fd00:1::103/128 dev lo
ip -6 addr add fd00:1::104/128 dev lo
ip -6 addr add fd00:1::105/128 dev lo

ip addr add 10.0.0.1 dev lo

echo ""
ifconfig lo
echo ""

while ! ping6 -c 2 -W 1 fd00::2; do
   echo "waiting for v6 gateway to remote tunnel endpoints be reachable ..."
   sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00:1::101 remote fd00:2::101
ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00:1::102 remote fd00:2::102
ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00:1::103 remote fd00:2::103
ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00:1::104 remote fd00:2::104
ip l2tp add tunnel tunnel_id 5 peer_tunnel_id 5 encap ip local fd00:1::105 remote fd00:2::105

ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65534 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65533 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65532 cookie $cookie peer_cookie $cookie l2spec_type none
ip l2tp add session tunnel_id 5 session_id 65531 peer_session_id 65531 cookie $cookie peer_cookie $cookie l2spec_type none

ip addr add 192.168.101.1 peer 192.168.101.2 dev l2tpeth0
ip addr add 192.168.102.1 peer 192.168.102.2 dev l2tpeth1
ip addr add 192.168.103.1 peer 192.168.103.2 dev l2tpeth2
ip addr add 192.168.104.1 peer 192.168.104.2 dev l2tpeth3
ip addr add 192.168.105.1 peer 192.168.105.2 dev l2tpeth4

ip link set dev l2tpeth0 up
ip link set dev l2tpeth1 up
ip link set dev l2tpeth2 up
ip link set dev l2tpeth3 up
ip link set dev l2tpeth4 up

ip route add 192.168.101.0/24 dev l2tpeth0
ip route add 192.168.102.0/24 dev l2tpeth1
ip route add 192.168.103.0/24 dev l2tpeth2
ip route add 192.168.104.0/24 dev l2tpeth3
ip route add 192.168.105.0/24 dev l2tpeth4

ip l2tp show tunnel
ip l2tp show session

echo ""
echo "installing xdp_router ..."
ulimit -l 1024
/sbin/xdp_loader --dev $interface --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/root/tunnels.sh | /sbin/xdp_tunnels --dev $interface
echo ""

SECODNS=0
for peerip in 192.168.101.2 192.168.102.2 192.168.103.2 192.168.104.2 192.168.105.2; do
   while ! ping -c 1 -W 1 $peerip; do
      echo "waiting for remote tunnel endpoint $peerip to be reachable ..."
      sleep 1
      if [ $SECONDS -gt 10 ]; then
         echo "ERROR: can't reach $peerip within 10 seconds."
         exit 1
      fi
   done
done

echo ""
echo "adding ecmp routes to remote v4 loopback via l2tp tunnels ..."
ip route add 10.0.0.0/8 proto static \
   nexthop dev l2tpeth0 via 192.168.101.2 weight 1 \
   nexthop dev l2tpeth1 via 192.168.102.2 weight 1 \
   nexthop dev l2tpeth2 via 192.168.103.2 weight 1 \
   nexthop dev l2tpeth3 via 192.168.104.2 weight 1 \
   nexthop dev l2tpeth4 via 192.168.105.2 weight 1

echo ""
echo "ip route:"
ip route 

echo ""
echo "flood ping to remote v4 loopback ..."
ping -f -c 1000 10.0.0.2

echo ""
echo "netstat -i:"
netstat -i

echo ""
echo "xdp_stats --dev $interface --single:"
xdp_stats --dev $interface --single

echo ""
echo "SUCCESS!"
if [ "no" == "$exit_container" ]; then
   /bin/bash
fi
exit 0
