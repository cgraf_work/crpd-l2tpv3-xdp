#!/bin/bash

interfaces="enp9s0f0 enp11s0f0 enp11s0f1"

set -e

echo ""
echo "removing xdp_router ..."
for int in $interfaces; do
  echo $int ...
  /sbin/xdp_loader --dev $int --unload
done
