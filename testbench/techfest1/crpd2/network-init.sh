#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

until ip link show enp11s0f3 up; do
   echo "waiting for interface enp11s0f3 up ..."
   sleep 1
done


ip -6 addr add fd00::2/128 dev lo
ip -6 addr add fd00::21/128 dev lo
ip -6 addr add fd00::22/128 dev lo
ip -6 addr add fd00::23/128 dev lo
ip -6 addr add fd00::24/128 dev lo
ip addr add 10.0.0.2/32 dev lo

ip -6 addr add fd00:3::1/64 dev enp11s0f2
ip -6 addr add fd00:4::1/64 dev enp11s0f3

ip addr add 10.253.0.1/24 dev enp11s0f2
ip addr add 10.254.0.1/24 dev enp11s0f3

#echo "starting avah-autoipd for enp9s0f1 ..."
#/usr/sbin/avahi-autoipd --no-drop-root --daemonize enp9s0f1

echo "adding static lladdr v6 neighbor and arp ..."
ip -6 neigh add fd00:3::2 lladdr 00:1b:21:99:b7:c8 nud permanent dev enp11s0f2
arp -s 10.253.0.2 00:1b:21:99:b7:c8
ip -6 neigh add fd00:4::2 lladdr 00:1b:21:99:b7:c9 nud permanent dev enp11s0f3
arp -s 10.254.0.2 00:1b:21:99:b7:c8

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00::11; do
  echo "waiting for fd00::11 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00::21 remote fd00::11
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.101.2/24 dev l2tpeth0
ip link set dev l2tpeth0 up

ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00::22 remote fd00::12
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.2/24 dev l2tpeth1
ip link set dev l2tpeth1 up

ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00::23 remote fd00::13
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.103.2/24 dev l2tpeth2
ip link set dev l2tpeth2 up

ip l2tp add tunnel tunnel_id 4 peer_tunnel_id 4 encap ip local fd00::24 remote fd00::14
ip l2tp add session tunnel_id 4 session_id 65532 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.104.2/24 dev l2tpeth3
ip link set dev l2tpeth3 up

/config/enable_xdp.sh

cores=5
ethtool -G enp9s0f1 rx 4096 || true
ethtool -G enp11s0f2 rx 4096 || true
ethtool -G enp11s0f3 rx 4096 || true
#ethtool -L enp9s0f1 combined $cores || true
#ethtool -L enp11s0f2 combined $cores || true
#ethtool -L enp11s0f3 combined $cores || true

echo "all set."
