#!/bin/bash
PCI1="0000:03:00.0"
PCI2="0000:03:00.1"
PCI3="0000:04:00.0"
PCI4="0000:04:00.1"

folder=$PWD/$(dirname $0)
cd $folder

set -e

echo "extracting pktgen from container ..."
mkdir -p dpdk
docker run --rm -v $PWD/dpdk:/u --user $(id -u):$(id -g) pktgen /bin/cp -r /dpdk/. /u
ls -l dpdk

echo ""
echo "installing kernel modules (needs sudo privileges)..."
sudo modprobe uio || true
sudo insmod dpdk/igb_uio.ko || true
sudo modprobe vfio-pci || true
sudo modprobe uio_pci_generic || true

echo ""
echo "binding $PCI1 and $PCI2 to dpdk ..."
sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI1
sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI1

#sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI2
#sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI2

sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI3
sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI3

#sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI4
#sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI4

sudo python3 dpdk/usertools/dpdk-devbind.py -s

echo "launching pktgen ..."
sudo dpdk/pktgen -- -T -P -m "[1:2].0, [5:6].1" -f pktgen-v4.pkt
#sudo dpdk/pktgen -- -T -P -m "[1:2].0, [3:4].1, [5:6].2, [7:8].3" -f pktgen-v4.pkt
sudo python3 dpdk/usertools/dpdk-devbind.py -b ixgbe $PCI1 $PCI2 $PCI3 $PCI4
