```
$ ./run.sh exit
=============================================================================================
test2: r1 -- veth -- r2, flood ping thru l2tpv3 tunnels using plain linux (with xdp)
=============================================================================================

975f8672d1a7d814b4ce1a1da6e3e600f996441f8b9fecfdb1f79669d1c19cef
Device "veth0" does not exist.
waiting for veth0 up ...
Device "veth0" does not exist.
waiting for veth0 up ...
192: veth0@if193: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 3000 qdisc noqueue state UP mode DEFAULT group default qlen 1000
    link/ether e2:69:aa:b4:2e:1f brd ff:ff:ff:ff:ff:ff link-netnsid 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 fd00:2::101  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::105  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::102  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::103  prefixlen 128  scopeid 0x0<global>
        inet6 fd00:2::104  prefixlen 128  scopeid 0x0<global>
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


PING fd00::1(fd00::1) 56 data bytes

--- fd00::1 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1012ms

waiting for v6 gateway to remote tunnel endpoints be reachable ...
PING fd00::1(fd00::1) 56 data bytes
64 bytes from fd00::1: icmp_seq=1 ttl=64 time=0.063 ms
64 bytes from fd00::1: icmp_seq=2 ttl=64 time=0.126 ms

--- fd00::1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1006ms
rtt min/avg/max/mdev = 0.063/0.094/0.126/0.031 ms
Tunnel 5, encap IP
  From fd00:2::105 to fd00:1::105
  Peer tunnel 5
Tunnel 4, encap IP
  From fd00:2::104 to fd00:1::104
  Peer tunnel 4
Tunnel 3, encap IP
  From fd00:2::103 to fd00:1::103
  Peer tunnel 3
Tunnel 2, encap IP
  From fd00:2::102 to fd00:1::102
  Peer tunnel 2
Tunnel 1, encap IP
  From fd00:2::101 to fd00:1::101
  Peer tunnel 1
Session 65531 in tunnel 5
  Peer session 65531, tunnel 5
  interface name: l2tpeth4
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65532 in tunnel 4
  Peer session 65532, tunnel 4
  interface name: l2tpeth3
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65533 in tunnel 3
  Peer session 65533, tunnel 3
  interface name: l2tpeth2
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65534 in tunnel 2
  Peer session 65534, tunnel 2
  interface name: l2tpeth1
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788
Session 65535 in tunnel 1
  Peer session 65535, tunnel 1
  interface name: l2tpeth0
  offset 0, peer offset 0
  cookie 1122334455667788  peer cookie 1122334455667788

installing xdp_router ...
Success: Loaded BPF-object(/root/xdp_router.o) and used section(xdp_l2tpv3)
 - XDP prog attached on device:veth0(ifindex:192)
 - Pinning maps in /sys/fs/bpf/veth0/

Collecting tunnels from BPF map
 - BPF map (bpf_map_type:1) id:168 name:xdp_tunnel_map key_size:4 value_size:32 max_entries:12
l2tpeth0(2): l2tp fd00:2::101 -> fd00:1::101
l2tpeth1(3): l2tp fd00:2::102 -> fd00:1::102
l2tpeth2(4): l2tp fd00:2::103 -> fd00:1::103
l2tpeth3(5): l2tp fd00:2::104 -> fd00:1::104
l2tpeth4(6): l2tp fd00:2::105 -> fd00:1::105

PING 192.168.101.1 (192.168.101.1) 56(84) bytes of data.
64 bytes from 192.168.101.1: icmp_seq=1 ttl=64 time=0.126 ms

--- 192.168.101.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.126/0.126/0.126/0.000 ms
PING 192.168.102.1 (192.168.102.1) 56(84) bytes of data.
64 bytes from 192.168.102.1: icmp_seq=1 ttl=64 time=0.098 ms

--- 192.168.102.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.098/0.098/0.098/0.000 ms
PING 192.168.103.1 (192.168.103.1) 56(84) bytes of data.
64 bytes from 192.168.103.1: icmp_seq=1 ttl=64 time=0.123 ms

--- 192.168.103.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.123/0.123/0.123/0.000 ms
PING 192.168.104.1 (192.168.104.1) 56(84) bytes of data.
64 bytes from 192.168.104.1: icmp_seq=1 ttl=64 time=0.101 ms

--- 192.168.104.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.101/0.101/0.101/0.000 ms
PING 192.168.105.1 (192.168.105.1) 56(84) bytes of data.
64 bytes from 192.168.105.1: icmp_seq=1 ttl=64 time=0.106 ms

--- 192.168.105.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.106/0.106/0.106/0.000 ms

adding ecmp routes to remote v4 loopback via l2tp tunnels ...

ip route:
10.0.0.0/8 proto static 
	nexthop via 192.168.101.1 dev l2tpeth0 weight 1 
	nexthop via 192.168.102.1 dev l2tpeth1 weight 1 
	nexthop via 192.168.103.1 dev l2tpeth2 weight 1 
	nexthop via 192.168.104.1 dev l2tpeth3 weight 1 
	nexthop via 192.168.105.1 dev l2tpeth4 weight 1 
192.168.101.0/24 dev l2tpeth0 scope link 
192.168.101.1 dev l2tpeth0 proto kernel scope link src 192.168.101.2 
192.168.102.0/24 dev l2tpeth1 scope link 
192.168.102.1 dev l2tpeth1 proto kernel scope link src 192.168.102.2 
192.168.103.0/24 dev l2tpeth2 scope link 
192.168.103.1 dev l2tpeth2 proto kernel scope link src 192.168.103.2 
192.168.104.0/24 dev l2tpeth3 scope link 
192.168.104.1 dev l2tpeth3 proto kernel scope link src 192.168.104.2 
192.168.105.0/24 dev l2tpeth4 scope link 
192.168.105.1 dev l2tpeth4 proto kernel scope link src 192.168.105.2 

flood ping to remote v4 loopback ...
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
--- 10.0.0.1 ping statistics ---
1000 packets transmitted, 1000 received, 0% packet loss, time 36ms
rtt min/avg/max/mdev = 0.008/0.016/0.066/0.005 ms, ipg/ewma 0.036/0.014 ms

netstat -i:
Kernel Interface table
Iface      MTU    RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
l2tpeth0  2934        4      0      0 0             5      0      0      0 BMRU
l2tpeth1  2934      714      0      0 0           715      0      0      0 BMRU
l2tpeth2  2934        4      0      0 0             6      0      0      0 BMRU
l2tpeth3  2934      917      0      0 0           918      0      0      0 BMRU
l2tpeth4  2934      380      1      1 0           383      0      0      0 BMRU
lo       65536        0      0      0 0             0      0      0      0 LRU
veth0     3000     2050      0      1 0          2049      0      0      0 BMRU

xdp_stats --dev veth0 --single:

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:167 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250581
XDP_DROP              22 pkts (         9 pps)           3 Kbytes (     0 Mbits/s) period:2.250633
XDP_PASS            2023 pkts (         2 pps)         331 Kbytes (     0 Mbits/s) period:2.250646
XDP_TX                32 pkts (        14 pps)           5 Kbytes (     0 Mbits/s) period:2.250667
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250679


flood ping to non-existing destination ...
PING 10.11.22.33 (10.11.22.33) 56(84) bytes of data.
....................................................................................................
--- 10.11.22.33 ping statistics ---
100 packets transmitted, 0 received, 100% packet loss, time 1649ms


xdp_stats --dev veth0 --single:

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:167 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.251059
XDP_DROP             125 pkts (         0 pps)          20 Kbytes (     0 Mbits/s) period:2.251070
XDP_PASS            2025 pkts (         0 pps)         331 Kbytes (     0 Mbits/s) period:2.251053
XDP_TX              6300 pkts (         0 pps)        1033 Kbytes (     0 Mbits/s) period:2.251040
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.251030

tx_pass_packets=6300 
SUCCESS!
SUMMARY test2: r1 -- veth -- r2, flood ping thru l2tpv3 tunnels using plain linux (with xdp): PASS
```
