#!/bin/bash

SECONDS=0

while true; do
  docker exec spine1 cli show isis adj | grep Up | wc -l | grep ^4$ >/dev/null && break
  echo "$SECONDS: waiting for all isis adjancencies up on spine1 ..."
  sleep 2
done
docker exec spine1 cli show isis adj
echo ""

while true; do
  docker exec spine1 cli show bgp summary | grep Estab | wc -l | grep ^8$ >/dev/null && break
  echo "$SECONDS: waiting for all established bgp sessions on spine1 ..."
  sleep 2
done
docker exec spine1 cli show bgp summary
echo ""

while true; do
  docker exec -ti r1 ping -c 1 10.0.0.1 && break
  echo "$SECONDS: waiting to reach spine1 from r1 via l2tpv3 ..."
  sleep 1
done
echo ""

while true; do
  docker exec -ti r1 ping -c 1 10.0.0.12 && break
  echo "$SECONDS: waiting to reach r2 from r1 via l2tpv3 ..."
  sleep 1
done
echo ""

while true; do
  docker exec -ti r1 ping -c 1 10.0.0.13 && break
  echo "$SECONDS: waiting to reach r3 from r1 via l2tpv3 ..."
  sleep 1
done

while true; do
  docker exec -ti r1 ping -c 1 10.0.0.14 && break
  echo "$SECONDS: waiting to reach r4 from r1 via l2tpv3 ..."
  sleep 1
done

while true; do
  docker exec -ti r3 ping -c 1 10.0.0.14 && break
  echo "$SECONDS: waiting to reach r4 from r3 via l2tpv3 ..."
  sleep 1
done

if ! docker exec -ti r1 xdp_stats -s -d eth0 ; then
  echo ""
  echo "xdp program missing on r1 !!"
  docker exec -ti r1 tail /root/network-init.log
  exit 1
fi

if ! docker exec -ti spine1 xdp_stats -s -d eth0 ; then
  echo ""
  echo "xdp program missing on spine1 !!"
  docker exec -ti spine1 tail /root/network-init.log
  exit 1
fi

echo ""
echo "show isis adj on spine1 ..."
docker exec spine1 cli show isis adj

echo ""
echo "show bgp summary on spine1 ..."
docker exec spine1 cli show bgp summary

echo ""
echo "show ip route hidden extensive on spine1 ..."
docker exec spine1 cli show route hidden extensive

echo ""
echo "success in $SECONDS seconds"
