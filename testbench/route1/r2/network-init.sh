#!/bin/bash

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::12/128 dev lo
ip addr add 10.0.0.12/32 dev lo

until ip link show eth0 up; do
  echo "waiting for eth0 up ..."
  sleep 1
done

/usr/sbin/avahi-autoipd --no-drop-root --daemonize eth0

echo ""
echo "installing xdp_l2tpv3 on eth0 ..."
ulimit -l 2048
/sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3

echo ""
echo "programming tx_ports map ..."
/sbin/xdp_tunnels --dev eth0 < /dev/null
