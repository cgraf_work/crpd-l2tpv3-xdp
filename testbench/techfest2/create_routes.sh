#!/bin/bash

#     route 10.1.0.0/16 next-hop 10.251.0.2;
#        route 10.2.0.0/16 next-hop 10.252.0.2;
#        route 10.3.0.0/16 next-hop 10.253.0.2;
#        route 10.4.0.0/16 next-hop 10.254.0.2;

for link in {1..4}; do
  for i in {0..255}; do
    for j in {0..255}; do
      echo "route 10.$link.$i.$j/32 next-hop 10.25$link.0.2;"
    done
  done
done

