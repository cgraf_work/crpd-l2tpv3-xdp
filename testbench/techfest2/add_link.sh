#!/bin/bash

MTU=3000

c1=$1
c2=$2
if [ -z "$c2" ]; then
    echo "$0 <container1> <container2>"
    exit 1
fi

set -e	# terminate on error

sudo mkdir -p /var/run/netns

SECONDS=0

until [ ! -z "$(docker ps -q -f name=$c1)" ]; do
   echo "waiting for container $c1 ..."
   sleep 1
   if [ $SECONDS -gt 5 ]; then
      echo "$c1 not running"
      exit 1
   fi
done

fc1=$(docker ps -q -f name=$c1)
echo "$c1 $fc1"
pid1=$(docker inspect -f "{{.State.Pid}}" $fc1)
if [ -z "$pid1" ]; then
    echo "Can't find pid for container $c1"
    exit 1
fi

until [ ! -z "$(docker ps -q -f name=$c2)" ]; do
   echo "waiting for container $c2 ..."
   sleep 1
   if [ $SECONDS -gt 5 ]; then
      echo "$c1 not running"
      exit 1
   fi
done

fc2=$(docker ps -q -f name=$c2)
echo "$c2 $fc2"
pid2=$(docker inspect -f "{{.State.Pid}}" $fc2)
if [ -z "$pid2" ]; then
    echo "Can't find pid for container $c2"
    exit 1
fi

echo "$c1 has pid $pid1"
echo "$c2 has pid $pid2"

sudo ln -sf /proc/$pid1/ns/net /var/run/netns/$c1
sudo ln -sf /proc/$pid2/ns/net /var/run/netns/$c2

#ifcount1=$(sudo ip netns exec $c1 ip link | grep ' eth' | wc -l)
#ifcount2=$(sudo ip netns exec $c2 ip link | grep ' eth' | wc -l)

#echo "$c1 has $ifcount1 eth interfaces"
#echo "$c2 has $ifcount2 eth interfaces"

sudo ip link del dev veth-${c1} 2>/dev/null || true

sudo ip link add veth-${c1} type veth peer name veth-${c2}

ifconfig veth-${c1}
ifconfig veth-${c2}

echo "setting mtu ..."
sudo ip link set dev veth-${c1} mtu $MTU
sudo ip link set dev veth-${c2} mtu $MTU

echo "moving endpoints to netns ..."
sudo ip link set veth-${c2} netns $c1
sudo ip link set veth-${c1} netns $c2

echo "bringing links up ..."
sudo ip netns exec $c1 ip link set up veth-${c2}
sudo ip netns exec $c2 ip link set up veth-${c1}

echo "$c1:veth-${c2} === $c2:veth-${c1}"
