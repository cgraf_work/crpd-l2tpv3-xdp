#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::1/128 dev lo
ip -6 addr add fd00:1::1/128 dev lo
ip -6 addr add fd00:1::2/128 dev lo
ip -6 addr add fd00:1::3/128 dev lo

ip addr add 10.0.0.1/32 dev lo

until ip link show eth2 up; do
  echo "waiting for eth2 up ..."
  sleep 1
done

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:1::11; do
  echo "waiting for fd00:1::11 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 1 peer_tunnel_id 1 encap ip local fd00:1::1 remote fd00:1::11
ip l2tp add session tunnel_id 1 session_id 65535 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.101.1 peer 192.168.101.2 dev l2tpeth0
ip link set dev l2tpeth0 up
ip route add 192.168.101.0/24 dev l2tpeth0

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:2::12; do
  echo "waiting for fd00:2::12 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 2 peer_tunnel_id 2 encap ip local fd00:1::2 remote fd00:2::12
ip l2tp add session tunnel_id 2 session_id 65534 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.102.1 peer 192.168.102.2 dev l2tpeth1
ip link set dev l2tpeth1 up
ip route add 192.168.102.0/24 dev l2tpeth1

echo "waiting for remote tunnel endpoint be reachable ..."
while ! ping6 -c 1 -W 1 fd00:3::13; do
  echo "waiting for fd00:3::13 ..."
  sleep 1
done

ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00:1::3 remote fd00:3::13
ip l2tp add session tunnel_id 3 session_id 65533 peer_session_id 65535 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.103.1 peer 192.168.103.2 dev l2tpeth2
ip link set dev l2tpeth2 up
ip route add 192.168.103.0/24 dev l2tpeth2

echo "adding l2tp tunnels ..."

ip l2tp show tunnel
ip l2tp show session

if [ -z "$XDP" ]; then
  echo ""
  echo "removing xdp on all interfaces ..."
  /sbin/xdp_loader --dev eth0 -U || true
  /sbin/xdp_loader --dev eth1 -U || true
  /sbin/xdp_loader --dev eth2 -U || true
else
  echo ""
  echo "installing xdp_l2tpv3 on all interfaces ..."
  ulimit -l 1024
  /sbin/xdp_loader --dev eth0 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
  /sbin/xdp_loader --dev eth1 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
  /sbin/xdp_loader --dev eth2 --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
  echo ""
  echo "set xdp tunnel map on all interfaces ..."
  /root/tunnels.sh | /sbin/xdp_tunnels --dev eth0
  /root/tunnels.sh | /sbin/xdp_tunnels --dev eth1
  /root/tunnels.sh | /sbin/xdp_tunnels --dev eth2
fi
