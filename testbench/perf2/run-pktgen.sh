#!/bin/bash
PCI="0000:09:00.0"    # ens6f0 on xeon

set -e

echo "extracting pktgen from container ..."
mkdir -p dpdk
docker run --rm -v $PWD/dpdk:/u --user $(id -u):$(id -g) pktgen /bin/cp -r /dpdk/. /u
ls -l dpdk

echo ""
echo "installing kernel modules (needs sudo privileges)..."
sudo modprobe uio || true
sudo insmod dpdk/igb_uio.ko || true
sudo modprobe vfio-pci || true
sudo modprobe uio_pci_generic || true

echo ""
echo "binding $interface to dpdk ..."
sudo python3 dpdk/usertools/dpdk-devbind.py --force -u $PCI
sudo python3 dpdk/usertools/dpdk-devbind.py -b igb_uio $PCI
sudo python3 dpdk/usertools/dpdk-devbind.py -s

echo "launching pktgen ..."
sudo dpdk/pktgen -- -T -P -m "2.[0]" -f pktgen-v4.pkt
