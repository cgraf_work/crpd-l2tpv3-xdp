#!/bin/bash

cookie="1122334455667788"

set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
ip -6 addr add fd00::13/128 dev lo
ip -6 addr add fd00:3::13/128 dev lo

ip addr add 10.0.0.13/32 dev lo

interface=enp12s0f1
until ip link show $interface up; do
  echo "waiting for $interface up ..." && sleep 1
done
ethtool --offload $interface rxvlan off txvlan off
ip link set dev $interface mtu 3000
ip link add link $interface name vlan300 type vlan id 300
ip -6 addr add fd00:3333::2/64 dev vlan300
ip link set dev vlan300 up
ip addr add 169.254.1.13/16 dev vlan300
while ! ping6 -c 1 -W 1 fd00:3333::1; do
  echo "waiting for fd00:3333::1 ..." && sleep 1
done
ip -6 route add fd00:1::3/128 via fd00:3333::1

while ! ping6 -c 1 -W 1 fd00:1::3; do
  echo "waiting for fd00:1::3 ..." && sleep 1
done
ip l2tp add tunnel tunnel_id 3 peer_tunnel_id 3 encap ip local fd00:3::13 remote fd00:1::3
ip l2tp add session tunnel_id 3 session_id 65535 peer_session_id 65533 cookie $cookie peer_cookie $cookie l2spec_type none
ip addr add 192.168.103.2 peer 192.168.103.1 dev l2tpeth0
ip link set dev l2tpeth0 up
ip route add 10.0.0.0/24 via 192.168.103.1 dev l2tpeth0 src 10.0.0.13

echo ""
echo "installing xdp_l2tpv3 on $interface ..."
ulimit -l 1024
/sbin/xdp_loader --dev $interface -N --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
echo ""
echo "set xdp tunnel map on $interface ..."
/root/tunnels.sh | /sbin/xdp_tunnels --dev $interface

echo "all set."
tail -f /dev/null
