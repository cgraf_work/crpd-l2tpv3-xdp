#!/bin/bash

crpd=cg_crpd_xdp_test                   # name of the cRPD container
crpd_image=cg_crpd_xpd                  # name of crpd image to be built. no need to have it equal to the container-name
i=veth0                                 # e.g. i=ens8f0 the IFD which shall be moved into the containers namespace to get XDP-enabled
i1=veth1
config_vol=cg_crpd_xdp_test_config

# clone repository
#git clone --recursive https://gitlab.com/cgraf_work/crpd-l2tpv3-xdp.git

# cd into cloned dir
#cd crpd-l2tpv3-xdp

# "building the XDP image - Dockerfile located in ./crpd-l2tpv3-xdp/xdp/"
#sudo docker build -t xdpbuild ./xdp

# >> you must have cRPD 20.2R1 docker images loaded as prerequiremt
# build the augmented cRPD image
# the used Dockerfile in crpd-l2tpv3-xdp/simple_beginners_setup/Dockerfile extracts the XDP binaries and modules
#cd simple_beginners_setup
#docker build -t $crpd_image .


# optional - create config-volume
docker volume create ${config_vol}
# launch the container

docker run --rm --detach --name $crpd -h $crpd --privileged --net=none --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${config_vol}:/config -it ${crpd_image}:latest

# create and move interfaces into the container. IFD like ens8f0 works as well, not only veth
ip link add dev $i type veth peer name $i1
ip link set dev $i up
ip link set dev $i1 up

./add_phy.sh $crpd $i
./add_phy.sh cg_crpd_xdp ${i1}

docker exec -ti $crpd ip link
docker exec -ti cg_crpd_xdp ip link

# final step - enable XDP on the IFD
docker exec -ti $crpd bash
i=veth0
mount -t bpf bpf /sys/fs/bpf/
ulimit -l 2048
/sbin/xdp_loader --dev $i --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_tunnels --dev $i </dev/null


