# simplistic IPv4/IPv6 setup

## Disclaimer
use at own risk

## Purpose
this is a very much simplified version of "https://gitlab.com/mwiget/crpd-l2tpv3-xdp"

Marcels version is very much more elegant by using docker-compose and make to have a single-stop to bring a full topology up and down

This version here is aimed to give a basic understanding how all of this is working

If you want more then a single cRPD as shown here in this repo, 
e.g. to built a larger test-topology, then please look at Marcels work.

Again, use any of this work at own risk



# quick summary of required steps

- build the XDP image
  - the cRPD container needs to be augmented with XDP binaries and XDP modules
  - those binaries and modules will get extracted from the XDP-image
  - no need to run any container based on the XDP-image (unless you manually want to extract the modules/binaries)
- build the augmented cRPD image
- launch the augmented cRPD image
- move interfaces into the cRPD's namespace
- once the container is aware of the interfaces
  - container self-configures interfaces
  - loads XDP-modules on desired interfaces

# requirements

- Ideally Ubuntu 20.04 with at least Kernel 5.4.0-42-generic
- Ubuntu 18.04 might work, but at least kernel 5.4.0-42-generic is a must
- Ubuntu 16.04 - do not waste time here..


# manual step-by-step instructions

```
crpd=cg_crpd_xdp_test                   # name of the cRPD container
crpd_image=cg_crpd_xpd                  # name of crpd image to be built. no need to have it equal to the container-name
i=veth0                                 # e.g. i=ens8f0 the IFD which shall be moved into the containers namespace to get XDP-enabled
i1=veth1
config_vol=cg_crpd_xdp_test_config

# clone repository
#git clone --recursive https://gitlab.com/cgraf_work/crpd-l2tpv3-xdp.git

# cd into cloned dir
#cd crpd-l2tpv3-xdp

# "building the XDP image - Dockerfile located in ./crpd-l2tpv3-xdp/xdp/"
#sudo docker build -t xdpbuild ./xdp

# >> you must have cRPD 20.2R1 docker images loaded as prerequiremt
# build the augmented cRPD image
# the used Dockerfile in crpd-l2tpv3-xdp/simple_beginners_setup/Dockerfile extracts the XDP binaries and modules
#cd simple_beginners_setup
#docker build -t $crpd_image .


# optional - create config-volume
docker volume create ${config_vol}
# launch the container

docker run --rm --detach --name $crpd -h $crpd --privileged --net=none --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${config_vol} -it ${crpd_image}:latest

# create and move interfaces into the container. IFD like ens8f0 works as well, not only veth
ip link add dev $i type veth peer name $i1
ip link set dev $i up
ip link set dev $i1 up

./add_phy.sh $crpd $i
./add_phy.sh cg_crpd_xdp ${i1}

docker exec -ti $crpd ip link
docker exec -ti $cg_crpd_xdp ip link
```

```
# final step - enable XDP on the IFD
docker exec -ti $crpd bash
i=veth0
mount -t bpf bpf /sys/fs/bpf/
ulimit -l 2048
/sbin/xdp_loader --dev $i --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
/sbin/xdp_tunnels --dev $i </dev/null
```


```
# verify from insdie the container
docker exec -ti $crpd bash
root@cg_crpd_xdp_test:~# ip l | grep xdp
40: veth0@if39: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 3000 xdp qdisc noqueue state UP mode DEFAULT group default qlen 1000
    prog/xdp id 249 tag 211b8b32cff4cc6a jited 

# show stats
xdp_stats -d $i

```


# one command to start it all

a single XDP augmented cRPD can be launched by starting this command

```
crpd-l2tpv3-xdp/simple_beginners_setup# ./crpd_startup.sh
```

below the README covers some files which should be adopted to match your env


# detailed setup

clone the Repository recursive

```
git clone --recursive https://gitlab.com/cgraf_work/crpd-l2tpv3-xdp.git


# make sure you cd'ed now into 
	cd crpd-l2tpv3-xdp/simple_beginners_setup
```

## prepare some files

### file: Dockerfile

The Dockerfile to augment the cRPD requires a cRPD docker image available

- make sure to have a cRPD docker image, e.g. 20.2R1
- used Dockerfile expects 20.2R1 - change the Dockerfile accordingly if you have another cRPD image installed

```
# check which cRPD image is installed on the host
simple_beginners_setup$ docker images | grep 20.2R1
crpd                20.2R1.10                          41d6ae0a8fcb        2 months ago        234MB


#adopt cRPD's Dockerfile to match the loaded docker image file
crpd-l2tpv3-xdp/simple_beginners_setup# cat Dockerfile | grep 20.2
FROM crpd:20.2R1.10                  <<<< adopt if needed to match an available cRPD image

```

### file crpd_startup: adopt the interfaces to add into's cRPD's namespace 

```
crpd-l2tpv3-xdp/simple_beginners_setup# cat crpd_startup.sh 
#!/bin/bash

crpd=cg_crpd_xdp                        # name of the cRPD container
crpd_image=cg_crpd_xpd                  # name of crpd image. no need to have it equal to the container-name

# adopt at least the variable interfaces
#                                       # used interfaces are SR-IOV enabled. this is not a must
#                                       # $interfaces is an array - use space_separated values. e.g. interfaces=( ens8f1v5 ens8f0v5 )
interfaces=( ens8f1v5 ens8f0v5 )        # this script moves the $interfaces into the crpd container
# end user defined vars

```

### file network-init.sh
```
crpd-l2tpv3-xdp/simple_beginners_setup/config# cat network-init.sh

#!/bin/bash
# interfaces and ip are an array (space separated values)
interfaces=( ens8f0v5 ens8f1v5 )
ipv4=( "192.168.20.1/24" "192.168.21.1/24" )
ipv6=( "fc00:1::1/64" "fc00:2::1/64" )
# if you do not want the loopbacks address configured, then set it to empty string, e.g. loopback_v4=""
loopback_v4="10.0.0.1/32"                       # set to empty string if you do not want this added
loopback_v6="fd00::1/128"                       # set to empty string if you do not want this added

```


## launch crpd

Once the above mentioned files (crpd_startup, network-init.sh, Dockerfile) got modified, we are ready to fire up everything with a single command

```
cd crpd-l2tpv3-xdp/simple_beginners_setup
sudo ./crpd_startup
```


### verification

the crpd_startup.sh script is entering the cRPD's containers shell

Configured interfaces as set in the network-init.sh should be seen:

```
root@cg_crpd_xdp:~# ip a s lo
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    inet 10.0.0.1/32 scope global lo
       valid_lft forever preferred_lft forever
    inet6 fd00::1/128 scope global
```

The XDP-enabled interfaces shall be indicated as:

```
# look for "prog/xdp"

root@cg_crpd_xdp:~# ip l  
...
47: ens8f0v5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 xdp qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 12:71:a9:75:17:3f brd ff:ff:ff:ff:ff:ff
    prog/xdp id 675 tag 211b8b32cff4cc6a jited 
56: ens8f1v5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 xdp qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 3a:62:78:2e:94:25 brd ff:ff:ff:ff:ff:ff
    prog/xdp id 683 tag 211b8b32cff4cc6a jited 
```

check for XDP statistics

```
root@cg_crpd_xdp:~# xdp_stats -d ens8f1v5

Collecting stats from BPF map
 - BPF map (bpf_map_type:6) id:72 name:xdp_stats_map key_size:4 value_size:16 max_entries:5
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250441
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250406
XDP_PASS            1068 pkts (         0 pps)         205 Kbytes (     0 Mbits/s) period:2.250398
XDP_TX                 0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250398
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.250398
```

# most important steps are documented in the crpd_startup.sh script itself

```
crpd-l2tpv3-xdp/simple_beginners_setup# cat crpd_startup.sh 

#!/bin/bash

crpd=cg_crpd_xdp                        # name of the cRPD container
crpd_image=cg_crpd_xpd                  # name of crpd image. no need to have it equal to the container-name

# adopt at least the variable interfaces
#                                       # used interfaces are SR-IOV enabled. this is not a must
#                                       # $interfaces is an array - use space_separated values. e.g. interfaces=( ens8f1v5 ens8f0v5 )
interfaces=( ens8f1v5 ens8f0v5 )        # this script moves the $interfaces into the crpd container
# end user defined vars


# check if root
if [ "$EUID" -ne 0 ]
then 
        echo "Please run as root"
        echo "exiting now"
        exit
fi

# check kernel-version
if lsb_release -a | grep 20.04
then
        echo "sounds like Ubuntu 20.04 running. good"
else
        echo "This stuff was tested on ubuntu 20.04"
        echo "ubuntu 18.04 might work, but at least kernel 5.4.0-42-generic shall be used (tbc)"
        echo "This is a warning only"
fi


# check if launched from desired directory
# setting directories
if echo $PWD | grep -E '*simple_beginners_setup'
then
        working_dir=$PWD
        main_dir=$(dirname $working_dir)
else
        echo "please cd into <simple_beginners_setup> dir"
        echo "exiting now"
fi

# checking if this script has already started a container. if yes, then stopping it now
if docker ps -a --format '{{.Names}}' | grep -Eq "^${crpd}\$"
then
        echo "stopping old running container"
        docker stop $crpd
fi


# build the XDP image
# we do need to augment the cRPD container with XDP modules and binaries
# first a XDP helper IMAGE is built
# no need to run a container based of this image
# when we build the cRPD docker image, the cRPD's Dockerfile will extract the needed stuff from this XDP-image
echo .
echo "building the XDP image"
sudo docker build -t xdpbuild ${main_dir}/xdp


# build the augmented cRPD image
# the used Dockerfile in crpd-l2tpv3-xdp/simple_beginners_setup/Dockerfile extracts the XDP binaries and modules
echo .
echo "building the cRPD image"
docker build -t $crpd_image ${working_dir}


# launch the container
# once container is launched, there is no need to "patch" afterwards. all XDP binaries and modules are already built-in
# make sure runit-init.sh command gets called with your "docker run" 
# the runinit-init.sh executes the script /config/network-init.sh to get xdp enabled on the interfaces
# further more, runinit-init.sh configures IP-addresses as desired
# note, the runinit-init.sh has a until-loop configured to only try configuring ip-addresses when those are visible in crps's namespace
# means the below add_phy script has already finished...
docker run --rm --detach --name $crpd -h $crpd --privileged --net=none --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${PWD}/config:/config -it ${crpd_image}:latest /sbin/runit-init.sh &

# 
# move interfaces into the container
# once container got startet, the add_phy script checks if the crpd container is running (until loop)
# then extract PID and of the cRPD container and move above defined $interfaces into the container
for i in "${interfaces[@]}"
do
        ./add_phy.sh $crpd $i
done


#login to crpd
# finally lets launch a shell inside the cRPD container
docker exec -ti $crpd bash

# done - have fun
```

