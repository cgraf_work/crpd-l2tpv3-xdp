#!/bin/bash


# interfaces and ip are an array (space separated values)
#interfaces=( "ens8f0v5.53" "ens8f1v5.53" )     # SR-IOV does not work with routing actually
#interfaces=( "ens8f0v5" "ens8f1v5" )		# SR-IOV does not ye work with routing
#interfaces=( "ens8f0" "ens8f1" )
#interfaces=( "ens8f0.53" "ens8f1.53" )
interfaces=( "ens8f0.22" "ens8f1.122" "ens8f0.23" "ens8f1.123" )
#ipv4=( "192.168.7.1/24" "192.168.5.1/24" )
ipv4=( "192.168.7.1/24" "192.168.5.1/24" "192.168.74.1/24" "192.168.54.1/24")
#ipv6=( "fc00:7::1/64" "fc00:5::1/64" )
ipv6=( "fc00:0:22::1/64" "fc00:1:122::1/64" "fc00:0:23::1/64" "fc00:1:123::1/64" )
# if you do not want the loopbacks address configured, then set it to empty string, e.g. loopback_v4=""
loopback_v4="192.168.53.1/32"                       # set to empty string if you do not want this added
loopback_v6="fc00:53::1/128"                        # set to empty string if you do not want this added


#set -e

mount -t bpf bpf /sys/fs/bpf/

echo "setting loopback ip addresses ..."
[ -z $loopback_v4 ] || ip addr add ${loopback_v4} dev lo
[ -z $loopback_v6 ] || ip -6 addr add ${loopback_v6} dev lo

# wait for add_phy script to add interfaces into crpds namespace
for i in "${interfaces[@]}"
do
	until ip link show $i; do
  		echo "waiting for $i up ..."
  	sleep 1
	done
done

# now all interfaces are available to the container
#lets add an ip-address and bring interface up
c=0
for i in "${interfaces[@]}"
do
	ifl_is_vlan="false"
	# need to check if vlan or not vlan interface
	# main reason is, >> explain this better..
	if [[ $i == *"."* ]]
	then
		# this is the vlan-case
		ifl_is_vlan="true"
		# given interface contains a dot. we take it as vlan
		ifd=$(echo $i | cut -f1 -d ".")
		vid=$(echo $i | cut -f2 -d ".")
		# bring up
		ip link set dev $ifd up
		ip link set dev $i up
		# set ip address on the $interfaces
		# unload XDP first
		/sbin/xdp_loader --dev $ifd -U || echo "xdp already removed"
		#vlan offload disable
		ethtool --offload $ifd rxvlan off txvlan off || echo "cant disable vlan offload on $ifd, must be done on $i"
		# loading xdp modules
		echo "installing xdp_l2tpv3 ..."
		ulimit -l 2048
		/sbin/xdp_loader --dev $ifd --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
		/sbin/xdp_tunnels --dev $ifd </dev/null

	else
		# this is the no-vlan-case
		# bring up
		ip link set dev $i up
		# set ip address on the $interfaces
		# unload XDP first
		/sbin/xdp_loader --dev $i -U || echo "xdp already removed"

		#vlan offload disable
		ethtool --offload $i rxvlan off txvlan off || echo "cant disable vlan offload on $i, must be done on $i"
		# loading xdp modules
		echo "installing xdp_l2tpv3 ..."
		ulimit -l 2048
		/sbin/xdp_loader --dev $i --auto-mode --force --filename /root/xdp_router.o --progsec xdp_l2tpv3
		/sbin/xdp_tunnels --dev $i </dev/null
	fi

	ip addr add ${ipv4[$c]} dev $i
	ip -6 addr add ${ipv6[$c]} dev $i

	c=$((c+1))
done




