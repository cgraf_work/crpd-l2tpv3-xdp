#!/bin/bash

crpd=cg_crpd_xdp   			# name of the cRPD container
crpd_image=cg_crpd_xpd			# name of crpd image. no need to have it equal to the container-name

# adopt at least the variable interfaces
# 					        # Note: SR-IOV interfaces not yet functional
#                                               # $interfaces is an array - use space_separated values. e.g. interfaces=( ens8f1v5 ens8f0v5 )
						# tagged interfaces shall work as well, e.g. interfaces=( "ens8f1v5.53" "ens8f0v5.53" )
#interfaces=( "ens8f1v5.53" "ens8f0v5.53" )	# SR-IOV not yet finctional .. this script moves the $interfaces into the crpd container
#interfaces=( "ens8f1v5" "ens8f0v5" )		#SR-IOV not yet finctional  .. # this script moves the $interfaces into the crpd container
#interfaces=( "ens8f1" "ens8f0" )		# this works fine.. this script moves the $interfaces into the crpd container
#interfaces=( "ens8f1.53" "ens8f0.53" "ens8f1.54" "ens8f0.54" )		# vlan-interfaces to be tested this script moves the $interfaces into the crpd container
interfaces=( "ens8f0.22" "ens8f1.122" "ens8f0.23" "ens8f1.123" )		# vlan-interfaces to be tested this script moves the $interfaces into the crpd container
# end user defined vars


# check if root
if [ "$EUID" -ne 0 ]
then 
	echo "Please run as root"
	echo "exiting now"
        exit
fi

# check kernel-version
if lsb_release -a | grep 20.04
then
	echo "sounds like Ubuntu 20.04 running. good"
else
	echo "This stuff was tested on ubuntu 20.04"
	echo "ubuntu 18.04 might work, but at least kernel 5.4.0-42-generic shall be used (tbc)"
	echo "This is a warning only"
fi
echo .

# check if launched from desired directory
# setting directories
if echo $PWD | grep -E '*simple_beginners_setup'
then
	working_dir=$PWD
	main_dir=$(dirname $working_dir)
else
	echo "please cd into <simple_beginners_setup> dir"
	echo "exiting now"
fi

container_uses_interfaces=false
# checking if this script has already started a container. if crpd already running, then stopping it now
if docker ps -a --format '{{.Names}}' | grep -Eq "^${crpd}\$"
then
	# move interfaces from the running container to default namespace
	for i in "${interfaces[@]}"
	do
		# do only perform the move if the interface is existing in the container
		if ip netns exec $crpd ip link show $i 
		then
			container_uses_interfaces=true
			echo "$i was used by container $crpd. moving $i into default namespace"
			ip netns exec $crpd ip link set $i netns 1
			echo .
		fi
	done

	# since interfaces are removed and already back in kernel we can stop the running container
	echo "stopping old running container"
  	docker stop $crpd
fi

# build the XDP image
# we do need to augment the cRPD container with XDP modules and binaries
# first a XDP helper IMAGE is built
# no need to run a container based of this image
# when we build the cRPD docker image, the cRPD's Dockerfile will extract the needed stuff from this XDP-image
echo .
echo "building the XDP image"
sudo docker build -t xdpbuild ${main_dir}/xdp


# build the augmented cRPD image
# the used Dockerfile in crpd-l2tpv3-xdp/simple_beginners_setup/Dockerfile extracts the XDP binaries and modules
echo .
echo "building the cRPD image"
docker build -t $crpd_image ${working_dir}


# launch the container
# once container is launched, there is no need to "patch" afterwards. all XDP binaries and modules are already built-in
# make sure runit-init.sh command gets called with your "docker run" 
# the runinit-init.sh executes the script /config/network-init.sh to get xdp enabled on the interfaces
# further more, runinit-init.sh configures IP-addresses as desired
# note, the runinit-init.sh has a until-loop configured to only try configuring ip-addresses when those are visible in crps's namespace
# means the below add_phy script has already finished...
docker run --rm --detach --name $crpd -h $crpd --privileged --net=none --sysctl net.ipv6.conf.all.disable_ipv6=0 --sysctl net.ipv6.conf.all.forwarding=1 -v ${PWD}/config:/config -it ${crpd_image}:latest /sbin/runit-init.sh &

# 
# move interfaces into the container
# once container got startet, the add_phy script checks if the crpd container is running (until loop)
# then extract PID and of the cRPD container and move above defined $interfaces into the container
echo .
declare -a adder=()
echo "moving interfaces into namespace"
for i in "${interfaces[@]}"
do
	# if interfaces have been previuosly in a container
	# then we need to await those beeing back in kernel
	if [[ $container_uses_interfaces == "true" ]]
	then
		until ip link show $i; do
			echo "waiting for $i to be back at default namespace ..."
			sleep 1
		done
	fi
	# if vlan does not yet exist, create it
	if [[ $i == *"."* ]]
	then
		echo "interface $i contain dot. assuming vlan"
		# given interface contains a dot. we take it as vlan
		ifd=$(echo $i | cut -f1 -d ".")
		vid=$(echo $i | cut -f2 -d ".")
		# check if vlan interface exists
		if ip link show type vlan dev $i
		then
	  		echo "interface $i exists. good"
		else
	  		echo "interface $i not existing. trying to create vlan-interface"
	  		ip link add link $ifd name $i type vlan id $vid
			# bringup procedure, vlan-offload, mtu all done in the network-init.sh script
		fi
		# in case $i refers to a vlan, move parental ifd as well into namespace
		echo .
		echo "move interface $ifd into $crpd namespace"
		#echo "debug press key"
		#read ee
		#${working_dir}/add_phy.sh $crpd $ifd
		adder+=($ifd)
		adder+=($i)
	else
		# its not a tagged vlan interface
		until ip link show $i; do
			echo "waiting for $i visible in kernel ..."
			sleep 1
		done

		# moving interface to namespace
		echo "move interface $i into $crpd namespace"
		echo "debug press key"
		read ee
		#${working_dir}/add_phy.sh $crpd $i
		adder+=($i)
	fi
done

echo "moving interfaces into the crpdcontainers namespace"
echo ${adder[@]}
for i in "${adder[@]}"
do
		${working_dir}/add_phy.sh $crpd $i
done
#login to crpd
# finally lets launch a shell inside the cRPD container
docker exec -ti $crpd bash

# done - have fun


