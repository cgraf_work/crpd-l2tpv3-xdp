#!/bin/bash

SECONDS=0

while true; do
  docker exec spine1 ifconfig | grep l2tp | wc -l | grep ^3$ >/dev/null && break
  echo "$SECONDS: waiting for all l2tpeth tunnels to be up on spine1 ..."
  sleep 2
done
docker exec spine1 ifconfig | grep l2tp
echo ""

echo "ping ipv4 loopbacks via tunnel from r1 ..."
while true; do
  docker exec -ti r1 ping -c 1 10.0.0.1 && break
  echo "$SECONDS: waiting to reach spine1 from r1 via l2tpv3 ..."
  sleep 1
done
echo ""

while true; do
  docker exec -ti r1 ping -c 1 10.0.0.12 && break
  echo "$SECONDS: waiting to reach r2 from r1 via l2tpv3 ..."
  sleep 1
done
echo ""

while true; do
  docker exec -ti r1 ping -c 1 10.0.0.13 && break
  echo "$SECONDS: waiting to reach r3 from r1 via l2tpv3 ..."
  sleep 1
done

echo "ping ipv6 loopbacks from r1 ..."
docker exec -ti r1 ping6 -c 3 fd00:1::1
docker exec -ti r1 ping6 -c 3 fd00:2::12
docker exec -ti r1 ping6 -c 3 fd00:3::13

echo "testing native routing from host1 ..."
docker exec -ti host1 ping -c 3 192.168.1.1
docker exec -ti host1 ping -c 3 192.168.2.1
docker exec -ti host1 ping -c 3 192.168.2.2

echo "testing v4 decap by pinging host1 from r1 ..."
docker exec -ti r1 ping -c 3 192.168.1.2
docker exec -ti r1 ping -c 3 192.168.2.2

echo "testing v6 decap by pinging host1 from r1 ..."
docker exec -ti r1 ping6 -c 3 fd00:11::2
docker exec -ti r1 ping6 -c 3 fd00:12::2
echo "testing v6 encap by pinging r1,r2 and r3 from host1 ..."
docker exec -ti host1 ping6 -c 3 fd00:1::11
docker exec -ti host1 ping6 -c 3 fd00:2::12
docker exec -ti host1 ping6 -c 3 fd00:3::13

echo ""
for instance in spine1 r1 r2 r3 host1 host2; do
  echo "collecting xdp stats on $instance ..."
  if ! docker exec -ti $instance xdp_stats -s -d eth0 ; then
    echo ""
    echo "xdp program missing on $instance !!"
    docker exec -ti $instance tail /root/network-init.log
    exit 1
  fi
done

echo ""
echo "show isis adj on spine1 ..."
docker exec spine1 cli show isis adj || docker exec spine1 vtysh -c "show isis neighbor"

echo ""
echo "show bgp summary on spine1 ..."
docker exec spine1 cli show bgp summary || docker exec spine1 vtysh -c "show bgp summary"

echo ""
echo "show ip route hidden extensive on spine1 ..."
docker exec spine1 cli show route hidden extensive || true

echo ""
echo "success in $SECONDS seconds"
