all: build

build:
	make -C xdp
	make -C l2tpv3router
	scripts/crpd_present.sh && make -C crpd_l2tpv3 || true
	scripts/crpd_present.sh && make -C pktgen || true

up: build down
	docker-compose up -d
	scripts/add_link.sh spine1 r1
	scripts/add_link.sh spine1 r2
	scripts/add_link.sh spine1 r3
	scripts/add_link.sh spine1 host1
	scripts/add_link.sh spine1 host2
	./validate.sh

frr: build down
	docker-compose -f docker-compose-frr.yml up -d
	scripts/add_link.sh spine1 r1
	scripts/add_link.sh spine1 r2
	scripts/add_link.sh spine1 r3
	scripts/add_link.sh spine1 host1
	scripts/add_link.sh spine1 host2
	./validate.sh

down:
	docker-compose down
	docker-compose -f docker-compose-frr.yml down
