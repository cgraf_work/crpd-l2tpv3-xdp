#!/bin/bash

iflist=$(ip l | grep -B2 /xdp | grep BROADCAST | awk '{print $2}' | cut -d: -f1)
for interface in $iflist; do
  sudo xdp/xdp_loader -U -d $interface
done
